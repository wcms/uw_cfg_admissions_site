<?php

/**
 * @file
 * uw_cfg_admissions_site.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function uw_cfg_admissions_site_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['uw_program_search-uw-program-search'] = array(
    'cache' => 8,
    'custom' => 0,
    'delta' => 'uw-program-search',
    'module' => 'uw_program_search',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'uw_admin_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_admin_theme',
        'weight' => 0,
      ),
      'uw_fdsu_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_fdsu_theme',
        'weight' => 0,
      ),
      'uw_theme_marketing' => array(
        'region' => 'mid_content',
        'status' => 1,
        'theme' => 'uw_theme_marketing',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
